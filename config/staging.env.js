'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"staging"',
  // override any settings in ./prod.env.js in here, e.g. the URL to your API
  API_URL: '"https://test.api.hpmc.jump.nl"'
})