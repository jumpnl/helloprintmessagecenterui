import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

const ApiService = {
  init () {
    Vue.use(VueAxios, axios)
    Vue.axios.defaults.baseURL = process.env.API_URL
  },

  query (resource, params) {
    return Vue.axios
      .get(resource, params)
      .catch((error) => {
        throw new Error(`[HPMC] ApiService ${error}`)
      })
  },

  get (resource, slug = '') {
    return Vue.axios
      .get(`${resource}/${slug}`)
      .catch((error) => {
        throw new Error(`[HPMC] ApiService ${error}`)
      })
  },

  list (resource) {
    return Vue.axios
      .get(`${resource}`)
      .catch((error) => {
        throw new Error(`[HPMC] ApiService ${error}`)
      })
  },

  post (resource, params) {
    return Vue.axios.post(`${resource}`, params)
  },

  update (resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params)
  },

  put (resource, params) {
    return Vue.axios
      .put(`${resource}`, params)
  },

  delete (resource) {
    return Vue.axios
      .delete(resource)
      .catch((error) => {
        throw new Error(`[HPMC] ApiService ${error}`)
      })
  }
}

export default ApiService

export const OrdersService = {
  jobs () {
    return ApiService.list('jobs')
  }
}

export const PspsService = {
  list () {
    return ApiService.list('psps')
  },
  create (params) {
    params['connector']['connectorCode'] = 'hpbox'
    return ApiService.post('psp', params)
  },
  delete (code) {
    return ApiService.delete(`psp/${code}`)
  }
}
