// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import { store } from './store'
import moment from 'moment'
import ApiService from '@/common/api.service'

Vue.use(Vuetify)

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment.unix(value).format('DD/MM/YYYY HH:mm:ss')
  }
});

Vue.config.productionTip = false

ApiService.init()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
