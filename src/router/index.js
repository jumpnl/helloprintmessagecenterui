import Vue from 'vue'
import Router from 'vue-router'

const routerOptions = [
  { path: '/', component: 'Orders' },
  { path: '/signin', component: 'Signin' },
  { path: '/orders', component: 'Orders' },
  { path: '/psps', component: 'Psps'}
]

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`@/components/${route.component}.vue`)
  }
})

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes
})
