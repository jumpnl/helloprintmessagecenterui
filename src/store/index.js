import Vue from 'vue'
import Vuex from 'vuex'
import order from './order.module'
import psp from './psp.module'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    appTitle: 'HPMC'
  },
  modules: {
    order,
    psp
  }
})