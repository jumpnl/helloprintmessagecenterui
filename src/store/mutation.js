import {
  FETCH_ORDERS_START,
  FETCH_ORDERS_END,
  FETCH_PSPS_START,
  FETCH_PSPS_END,
  CREATE_PSP,
  DELETE_PSP
} from './mutation.types'
  
export const orderMutations = {
  [FETCH_ORDERS_START] (state) {},
  [FETCH_ORDERS_END] (state, payload) {
    state.orders = payload
  }
}

export const pspMutations = {
  [FETCH_PSPS_START] (state) {},
  [FETCH_PSPS_END] (state, payload) {
    state.psps = payload
  }
}