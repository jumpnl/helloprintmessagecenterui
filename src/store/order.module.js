import {
  OrdersService
} from '@/common/api.service'

import {
  FETCH_ORDERS
} from './action.types'

import {
  FETCH_ORDERS_START,
  FETCH_ORDERS_END
} from './mutation.types'

const state = {
  jobs: [],
  isLoading: true,
}

const getters = {
  jobs (state) {
    return state.jobs
  },
  isLoading (state) {
    return state.isLoading
  }
}

const actions = {
  [FETCH_ORDERS] ({ commit }, params) {
    commit(FETCH_ORDERS_START)
    return OrdersService.jobs()
      .then(({ data }) => {
        commit(FETCH_ORDERS_END, data)
      })
      .catch((error) => {
        throw new Error(error)
      })
  }
}

const mutations = {
  [FETCH_ORDERS_START] (state) {
    state.isLoading = true
  },
  [FETCH_ORDERS_END] (state, { jobs }) {
    state.jobs = jobs
    state.isLoading = false
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
