import {
  PspsService
} from '@/common/api.service'

import {
  FETCH_PSPS
} from './action.types'

import {
  FETCH_PSPS_START,
  FETCH_PSPS_END,
  CREATE_PSP,
  DELETE_PSP
} from './mutation.types'

const state = {
  psps: [],
  pspsIsLoading: true,
}

const getters = {
  psps (state) {
    return state.psps
  },
  pspsIsLoading (state) {
    return state.pspsIsLoading
  }
}

const actions = {
  [FETCH_PSPS] ({ commit }, params) {
    commit(FETCH_PSPS_START)
    return PspsService.list()
      .then(({ data }) => {
        commit(FETCH_PSPS_END, data)
      })
      .catch((error) => {
        throw new Error(error)
      })
  },
  [CREATE_PSP] (context, payload) {
    return PspsService.create(payload)
      .then(() => { context.dispatch(FETCH_PSPS, 1) })
      .catch((error) => {
        throw new Error(error)
      })
  },
  [DELETE_PSP] (context, payload) {
    return PspsService.delete(payload)
      .then(() => { context.dispatch(FETCH_PSPS, 1) })
      .catch((error) => {
        throw new Error(error)
      })
  }
}

const mutations = {
  [FETCH_PSPS_START] (state) {
    state.pspsIsLoading = true
  },
  [FETCH_PSPS_END] (state, { psps }) {
    state.psps = psps
    state.pspsIsLoading = false
  }

}

export default {
  state,
  getters,
  actions,
  mutations
}
